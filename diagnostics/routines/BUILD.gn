# Copyright 2019 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import("//common-mk/pkg_config.gni")

# The urandom tool executable.
pkg_config("urandom_pkg_deps") {
  pkg_deps = [
    "libbrillo",
    "libchrome-${libbase_ver}",
  ]
}

executable("urandom") {
  configs += [ ":urandom_pkg_deps" ]
  sources = [
    "urandom/main.cc",
  ]
}

# The smartctl-check tool executable.
pkg_config("smartctl-check_pkg_deps") {
  pkg_deps = [
    "libbrillo",
    "libchrome-${libbase_ver}",
    "libdebugd-client",
  ]
}

executable("smartctl-check") {
  configs += [ ":smartctl-check_pkg_deps" ]
  sources = [
    "smartctl_check/main.cc",
    "smartctl_check/smartctl_check_utils.cc",
  ]
}

# The floating-point-accuracy tool executable.
pkg_config("floating-point-accuracy_pkg_deps") {
  pkg_deps = [
    "libbrillo-${libbase_ver}",
    "libchrome-${libbase_ver}",
  ]
}

# The prime_search tool executable.
pkg_config("prime_search_pkg_deps") {
  pkg_deps = [
    "libbrillo",
    "libchrome-${libbase_ver}",
  ]
}

executable("floating-point-accuracy") {
  configs += [ ":floating-point-accuracy_pkg_deps" ]
  cflags_cc = [
    "-msse",
    "-mno-mmx",
  ]
  sources = [
    "floating_point/main.cc",
  ]
}

executable("prime-search") {
  configs += [ ":prime_search_pkg_deps" ]
  sources = [
    "prime_search/main.cc",
    "prime_search/prime_number_search.cc",
  ]
}

# Library that provides the diagnostic routine interface.
pkg_config("libdiag_routine_pkg_deps") {
  pkg_deps = [
    "libbrillo",
    "libchrome-${libbase_ver}",
  ]
}

source_set("libdiag_routine") {
  deps = [
    "//diagnostics:diagnostics_mojo_bindings",
    "//diagnostics/common:libcommon",
  ]
  all_dependent_configs = [ ":libdiag_routine_pkg_deps" ]
  sources = [
    "ac_power/ac_power.cc",
    "battery/battery.cc",
    "battery_discharge/battery_discharge.cc",
    "battery_discharge/battery_discharge_constants.cc",
    "battery_sysfs/battery_sysfs.cc",
    "cpu_cache/cpu_cache.cc",
    "cpu_stress/cpu_stress.cc",
    "diag_process_adapter_impl.cc",
    "disk_read/disk_read.cc",
    "floating_point/floating_point_accuracy.cc",
    "nvme_self_test/nvme_self_test.cc",
    "nvme_wear_level/nvme_wear_level.cc",
    "prime_search/prime_search.cc",
    "smartctl_check/smartctl_check.cc",
    "subproc_routine.cc",
    "urandom/urandom.cc",
  ]
}

if (use.test) {
  # Libraries for unit tests.
  pkg_config("libroutine_test_utils_pkg_deps") {
    pkg_deps = [
      "libchrome-${libbase_ver}",
      "libmojo-${libbase_ver}",
    ]
  }

  source_set("libroutine_test_utils") {
    deps = [
      "//diagnostics:diagnostics_mojo_bindings",
    ]
    configs += [
      ":libroutine_test_utils_pkg_deps",
      "//common-mk:test",
    ]
    sources = [
      "routine_test_utils.cc",
    ]
  }

  pkg_config("routine_test_pkg_deps") {
    pkg_deps = [
      "libchrome-${libbase_ver}",
      "libchrome-test-${libbase_ver}",
    ]
  }

  executable("routine_test") {
    configs += [
      "//common-mk:test",
      ":routine_test_pkg_deps",
    ]
    deps = [
      ":libdiag_routine",
      ":libroutine_test_utils",
      "../../common-mk/testrunner:testrunner",
      "//diagnostics/common:libcommon",
      "//diagnostics/common:libcommon_test_utils",
    ]
    sources = [
      "ac_power/ac_power_test.cc",
      "battery/battery_test.cc",
      "battery_discharge/battery_discharge_test.cc",
      "battery_sysfs/battery_sysfs_test.cc",
      "nvme_self_test/nvme_self_test_test.cc",
      "nvme_wear_level/nvme_wear_level_test.cc",
      "prime_search/prime_number_search.cc",
      "prime_search/prime_number_search_test.cc",
      "smartctl_check/smartctl_check_utils.cc",
      "smartctl_check/smartctl_check_utils_test.cc",
      "subproc_routine_test.cc",
    ]
  }
}
